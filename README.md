# 简介
欢迎使用荔诚科技开发者工具套件（SDK 1.0），SDK 1.0是荔诚云平台的配套工具。目前已经支持认证中心，消息中心，存储中心等产品，后续所有的服务产品都会接纳进来。实现了统一化，具有多种语言版本的SDK使用方法相同，接口调用方式相同，统一的错误码和返回包格式这些优点。为方便JAVA开发者调试和接收荔诚云产品API，此处向您介绍适用于Java的荔诚云开发工具包，并提供首次使用开发工具包的简单示例。让您快速获取荔诚云Java SDK并开始调用。

# 依赖环境
1. 依赖环境：JDK 7版本及更高版本。
2. 从荔诚云控制台开通相应产品。
3. 获取SecretID，SecretKey以及调用地址（端点）

# 获取安装
在第一次使用云API之前，用户首先需要在荔诚云控制台上申请安全凭证，安全凭证包括SecretID和SecretKey，SecretID是用于标识API调用者的身份， SecretKey是用于加密密钥和服务器端验证签名的密钥SecretKey必须严格保管，避免替换。

## 通过二进制包安装（推荐）
1. 前往 [Gitee 代码托管地址](https://gitee.com/luxor/luxor-sdk-java) 下载原始码压缩包；
2. 解压二进制包到您项目合适的位置；
3. 需要将vendor目录下的jar包放在java的可找到的路径中；
4. 通过maven进行引用，参考如下例子：
```
<dependency>
    <groupId>org.luxor</groupId>
    <artifactId>luxor-sdk-java</artifactId>
    <version>1.0.0</version>
    <scope>system</scope>
    <systemPath>${project.basedir}/src/main/resources/lib/luxor-sdk-java-1.0.0.jar</systemPath>
</dependency>
```

## 通过Maven安装（仅适合公司内部项目）
过Maven获取安装是使用JAVA SDK的推荐方法，Maven是JAVA的依赖管理工具，支持您项目所需的依赖项，将其安装到项目中。关于Maven详细可参考Maven官网。
```
<dependency>
    <groupId>org.luxor</groupId>
    <artifactId>luxor-sdk-java</artifactId>
    <version>1.0.0</version>
</dependency>
```
公司内部用户可以使用镜像源加速下载，编辑 maven 的 settings.xml 配置文件，在 mirrors 段落增加镜像配置：
```
<mirror>
    <id>luxor</id>
    <name>luxor maven mirror</name>
    <url>http://172.168.1.206:7000/repository/maven-public/</url>
    <mirrorOf>*</mirrorOf>
</mirror>
```

# 示例

以查询实例接口DescribeInstances为例：

#### 简化版
```
todo
```

# 相关配置

## 代理
可以单独设置HTTP代理
```
    HttpProfile httpProfile = new HttpProfile();
    httpProfile.setProxyHost("真实代理ip");
    httpProfile.setProxyPort(真实代理端口);
```
## 支持http
SDK支持http协议和https协议，通过设置HttpProfile的setProtocol（）方法可以实现协议间的切换：
```
    HttpProfile httpProfile = new HttpProfile();
    httpProfile.setProtocol("http://"); //http协议
    httpProfile.setProtocol("https://"); //https协议
```
## 支持打印日志
SDK支持打印日志。首先，在创建CLientProfile对象时，设置debug模式为真，会打印sdk异常信息和流量信息
```
    ClientProfile clientProfile = new ClientProfile();
    clientProfile.setDebug(true);
```

## 支持重试请求
SDK支持重试请求。对于每一个请求，您可以设置重试次数，如果接口请求未成功，就进行重试，直到请求成功或者达到重试次数为止。待设置的重试次数最大为10，最小为0，每次重试失败需要睡眠1秒钟。
