/*
 * Copyright (c) 2018 THL A29 Limited, a Tencent company. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package org.luxor.sdk.common.http;

import okhttp3.*;
import org.luxor.sdk.common.exception.LuxorCloudSDKException;

import java.io.IOException;
import java.net.Proxy;
import java.util.concurrent.TimeUnit;

/**
 * @author Mr.Yan
 **/
public class HttpConnection {

    private OkHttpClient client;

    public HttpConnection(Integer connTimeout, Integer readTimeout, Integer writeTimeout) {
        this.client = new OkHttpClient.Builder()
                .connectTimeout(connTimeout, TimeUnit.SECONDS)
                .readTimeout(readTimeout, TimeUnit.SECONDS)
                .writeTimeout(writeTimeout, TimeUnit.SECONDS)
                .build();
    }

    public void addInterceptors(Interceptor interceptor) {
        this.client = this.client.newBuilder().addInterceptor(interceptor).build();
    }

    public void setProxy(Proxy proxy) {
        this.client = this.client.newBuilder().proxy(proxy).build();
    }

    public void setProxyAuthenticator(Authenticator authenticator) {
        this.client = this.client.newBuilder().proxyAuthenticator(authenticator).build();
    }

    public Response doRequest(Request request) throws LuxorCloudSDKException {
        try {
            return this.client.newCall(request).execute();
        } catch (IOException e) {
            throw new LuxorCloudSDKException(e.getClass().getName() + "-" + e.getMessage());
        }
    }

    public Response getRequest(String url, Headers headers) throws LuxorCloudSDKException {
        try {
            Request request = new Request.Builder().url(url).get().headers(headers).build();
            return this.doRequest(request);
        } catch (IllegalArgumentException e) {
            throw new LuxorCloudSDKException(e.getClass().getName() + "-" + e.getMessage());
        }
    }

    public Response postRequest(String url, byte[] body, Headers headers) throws LuxorCloudSDKException {
        try {
            RequestBody requestBody = RequestBody.create(MediaType.parse(headers.get("Content-Type")), body);
            Request request = new Request.Builder().url(url).post(requestBody).headers(headers).build();
            return this.doRequest(request);
        } catch (IllegalArgumentException e) {
            throw new LuxorCloudSDKException(e.getClass().getName() + "-" + e.getMessage());
        }
    }

    public Response deleteRequest(String url, byte[] body, Headers headers) throws LuxorCloudSDKException {
        try {
            RequestBody requestBody = RequestBody.create(MediaType.parse(headers.get("Content-Type")), body);
            Request request = new Request.Builder().url(url).delete(requestBody).headers(headers).build();
            return this.doRequest(request);
        } catch (IllegalArgumentException e) {
            throw new LuxorCloudSDKException(e.getClass().getName() + "-" + e.getMessage());
        }
    }

    public Response putRequest(String url, byte[] body, Headers headers) throws LuxorCloudSDKException {
        try {
            RequestBody requestBody = RequestBody.create(MediaType.parse(headers.get("Content-Type")), body);
            Request request = new Request.Builder().url(url).put(requestBody).headers(headers).build();
            return this.doRequest(request);
        } catch (IllegalArgumentException e) {
            throw new LuxorCloudSDKException(e.getClass().getName() + "-" + e.getMessage());
        }
    }

    public Response patchRequest(String url, byte[] body, Headers headers) throws LuxorCloudSDKException {
        try {
            RequestBody requestBody = RequestBody.create(MediaType.parse(headers.get("Content-Type")), body);
            Request request = new Request.Builder().url(url).patch(requestBody).headers(headers).build();
            return this.doRequest(request);
        } catch (IllegalArgumentException e) {
            throw new LuxorCloudSDKException(e.getClass().getName() + "-" + e.getMessage());
        }
    }


}
