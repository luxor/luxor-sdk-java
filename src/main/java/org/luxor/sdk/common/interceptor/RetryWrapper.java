package org.luxor.sdk.common.interceptor;


import okhttp3.Request;
import okhttp3.Response;

import java.util.Arrays;
import java.util.List;

/**
 * @author Mr.yan
 */
public class RetryWrapper {
    private final static List<Integer> SKIP_CODES = Arrays.asList(301, 302, 401, 403, 404, 502, 504);
    private volatile int retryNum = 0;
    private int maxRetry;
    private Request request;
    private Response response;

    public RetryWrapper(Request request, int maxRetry) {
        this.request = request;
        this.maxRetry = maxRetry;
    }

    public RetryWrapper(Response response) {
        this.response = response;
    }

    public int getRetryNum() {
        return retryNum;
    }

    public void setRetryNum(int retryNum) {
        this.retryNum = retryNum;
    }

    public Request getRequest() {
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

    public boolean isSuccessful() {
        return response != null && response.isSuccessful();
    }

    public boolean isNeedReTry() {
        if (SKIP_CODES.contains(getResponse().code())) {
            return false;
        }
        return !isSuccessful() && retryNum < maxRetry;
    }
}
