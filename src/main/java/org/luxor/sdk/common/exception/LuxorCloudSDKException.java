package org.luxor.sdk.common.exception;

/**
 * @author Mr.Yan
 **/
public class LuxorCloudSDKException extends Exception {

    private static final long serialVersionUID = 1L;

    /**
     * UUID of the request, it will be empty if request is not fulfilled.
     */
    private final String requestId;

    /**
     * Error code, When API returns a failure, it must have an error code.
     */
    private final String errorCode;

    public LuxorCloudSDKException(String message) {
        this(message, "");
    }

    public LuxorCloudSDKException(String message, String requestId) {
        this(message, requestId, "");
    }

    public LuxorCloudSDKException(String message, String requestId, String errorCode) {
        super(message);
        this.requestId = requestId;
        this.errorCode = errorCode;
    }

    public String getRequestId() {
        return requestId;
    }

    /**
     * Get error code
     *
     * @return A string represents error code
     */
    public String getErrorCode() {
        return errorCode;
    }

    @Override
    public String toString() {
        return "[LuxorCloudSDKException] "
                + " code:"
                + this.getErrorCode()
                + " message:"
                + this.getMessage()
                + " requestId:"
                + this.getRequestId();
    }
}
