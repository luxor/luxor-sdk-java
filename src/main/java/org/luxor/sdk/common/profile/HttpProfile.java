package org.luxor.sdk.common.profile;

/**
 * @author Mr.yan
 */
public class HttpProfile {

    /**
     * Time unit, 1 minute, equals 60 seconds.
     */
    public static final int TM_MINUTE = 60;

    /**
     * HTTPS or HTTP, currently only HTTPS is valid.
     */
    private HttpProtocol protocol;

    /**
     * Endpoint means the host which this request is sent to, such as sso.luxor-cms.cn.
     */
    private String host;

    /**
     * Endpoint means the port which this request is sent to, such as 80.
     */
    private int port;

    /**
     * Read timeout in seconds.
     */
    private int readTimeout;

    /**
     * Write timeout in seconds
     */
    private int writeTimeout;

    /**
     * Connect timeout in seconds
     */
    private int connTimeout;

    /**
     * http proxy
     */
    private HttpProxy proxy;

    public HttpProfile() {
        this.protocol = HttpProtocol.HTTPS;
        this.host = "luxor-cms.cn";
        this.port = 80;
        this.readTimeout = 0;
        this.writeTimeout = 0;
        this.connTimeout = HttpProfile.TM_MINUTE;
    }

    public HttpProfile(HttpProtocol protocol, String host, int port) {
        this.protocol = protocol;
        this.host = host;
        this.port = port;
        this.readTimeout = 0;
        this.writeTimeout = 0;
        this.connTimeout = HttpProfile.TM_MINUTE;
    }

    public HttpProtocol getHttpProtocol() {
        return protocol;
    }

    public String getDomain() {
        if (this.getPort() == 80) {
            return this.getHost();
        } else {
            return this.getHost() + ":" + this.getPort();
        }
    }

    public String getUrl() {
        return this.getHttpProtocol() + this.getDomain();
    }

    public void setReqProtocol(HttpProtocol protocol) {
        this.protocol = protocol;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public int getReadTimeout() {
        return readTimeout;
    }

    public void setReadTimeout(int readTimeout) {
        this.readTimeout = readTimeout;
    }

    public int getWriteTimeout() {
        return writeTimeout;
    }

    public void setWriteTimeout(int writeTimeout) {
        this.writeTimeout = writeTimeout;
    }

    public int getConnTimeout() {
        return connTimeout;
    }

    public void setConnTimeout(int connTimeout) {
        this.connTimeout = connTimeout;
    }

    public HttpProxy getProxy() {
        return proxy;
    }

    public void setProxy(HttpProxy proxy) {
        this.proxy = proxy;
    }

    @Override
    public String toString() {
        return "HttpProfile{" +
                "protocol=" + protocol +
                ", host='" + host +
                ", port=" + port +
                ", readTimeout=" + readTimeout +
                ", writeTimeout=" + writeTimeout +
                ", connTimeout=" + connTimeout +
                ", proxy=" + proxy +
                '}';
    }
}
