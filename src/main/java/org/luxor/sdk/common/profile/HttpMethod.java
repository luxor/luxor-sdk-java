package org.luxor.sdk.common.profile;

/**
 * @author Mr.yan  @date 2021/9/8
 */
public enum HttpMethod {
    POST("POST"),
    GET("GET"),
    PUT("PUT"),
    DELETE("DELETE"),
    PATCH("PATCH");

    private final String method;

    HttpMethod(String method) {
        this.method = method;
    }

    @Override
    public String toString() {
        return this.method;
    }

    public String getValue() {
        return this.method;
    }
}
