package org.luxor.sdk.common.profile;

/**
 * API接口签名算法
 *
 * @author Mr.yan  @date 2021/9/8
 */
public enum SignAlgorithm {

    /**
     * Signature process version 1, with HmacSHA1.
     */
    SIGN_SHA1("HmacSHA1"),
    /**
     * Signature process version 1, with HmacSHA256.
     */
    SIGN_SHA256("HmacSHA256");

    private final String signMethod;

    SignAlgorithm(String signMethod) {
        this.signMethod = signMethod;
    }

    @Override
    public String toString() {
        return this.signMethod;
    }

    public String getValue() {
        return this.signMethod;
    }
}
