package org.luxor.sdk.common.profile;

/**
 * @author Mr.Yan
 */
public enum HttpProtocol {
    HTTPS("https://"),
    HTTP("http://");

    private final String protocol;

    HttpProtocol(String protocol) {
        this.protocol = protocol;
    }

    @Override
    public String toString() {
        return this.protocol;
    }

    public String getValue() {
        return this.protocol;
    }
}
