package org.luxor.sdk.common.profile;

/**
 * 客户端概述
 *
 * @author Mr.yan
 */
public class ClientProfile {

    private HttpProfile httpProfile;

    private SignAlgorithm signAlgorithm;

    private boolean debug;

    public ClientProfile(HttpProfile httpProfile, SignAlgorithm signAlgorithm) {
        if (signAlgorithm == null) {
            signAlgorithm = SignAlgorithm.SIGN_SHA256;
        }
        if (httpProfile == null) {
            httpProfile = new HttpProfile();
        }
        this.debug = false;
        this.httpProfile = httpProfile;
        this.signAlgorithm = signAlgorithm;
    }

    public ClientProfile(SignAlgorithm signAlgorithm) {
        this(new HttpProfile(), signAlgorithm);
    }

    public ClientProfile() {
        this(new HttpProfile(), SignAlgorithm.SIGN_SHA256);
    }

    public HttpProfile getHttpProfile() {
        return httpProfile;
    }

    public void setHttpProfile(HttpProfile httpProfile) {
        this.httpProfile = httpProfile;
    }

    public SignAlgorithm getSignMethod() {
        return signAlgorithm;
    }

    public void setSignMethod(SignAlgorithm signAlgorithm) {
        this.signAlgorithm = signAlgorithm;
    }

    public boolean isDebug() {
        return debug;
    }

    public void setDebug(boolean debug) {
        this.debug = debug;
    }

    @Override
    public String toString() {
        return "ClientProfile{" +
                "httpProfile=" + httpProfile +
                ", signMethod=" + signAlgorithm +
                ", debug=" + debug +
                '}';
    }
}
