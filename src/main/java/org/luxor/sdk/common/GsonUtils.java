package org.luxor.sdk.common;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;

import java.util.List;
import java.util.Map;

/**
 * Json 工具类
 *
 * @author Mr.Yan
 **/
public class GsonUtils {
    private static final Gson INSTANCE;
    private static final Gson INSTANCE_PRETTY;

    static {
        INSTANCE = new GsonBuilder().enableComplexMapKeySerialization()
                // 解决long类型精度丢失问题
                .setLongSerializationPolicy(LongSerializationPolicy.STRING)
                // 指定返回的时间格式
                .setDateFormat("yyyy-MM-dd HH:mm:ss")
                // 禁止转义html标签
                .disableHtmlEscaping()
                .create();

        INSTANCE_PRETTY = new GsonBuilder().enableComplexMapKeySerialization()
                // 解决long类型精度丢失问题
                .setLongSerializationPolicy(LongSerializationPolicy.STRING)
                // 指定返回的时间格式
                .setDateFormat("yyyy-MM-dd HH:mm:ss")
                // 禁止转义html标签
                .disableHtmlEscaping()
                // 格式化输出
                .setPrettyPrinting()
                .create();
    }

    /**
     * 对象转换为JsonText
     */
    public static String toJson(Object object) throws JsonIOException {
        if (object == null) {
            return null;
        }
        return INSTANCE.toJson(object);
    }

    /**
     * 对象转换为格式化的JsonText
     */
    public static String toJsonPretty(Object object) throws JsonIOException {
        return INSTANCE_PRETTY.toJson(object);
    }

    /**
     * JsonText转换为JavaBean
     */
    public static <T> T toBean(String jsonText, Class<T> clazz) throws JsonSyntaxException {
        if (jsonText == null || jsonText.trim().isEmpty() || clazz == null) {
            return null;
        }
        return INSTANCE.fromJson(jsonText, clazz);
    }

    /**
     * 转换为List
     */
    public static <T> List<T> toList(String jsonText, Class<T> clazz) throws JsonSyntaxException {
        if (jsonText == null || clazz == null) {
            return null;
        }
        return INSTANCE.fromJson(jsonText, new TypeToken<List<T>>() {
        }.getType());
    }

    /**
     * 转换为Map
     */
    public static Map<String, Object> toMap(String jsonText) throws JsonSyntaxException {
        if (jsonText == null) {
            return null;
        }
        return INSTANCE.fromJson(jsonText, new TypeToken<Map<String, Object>>() {
        }.getType());
    }

}
