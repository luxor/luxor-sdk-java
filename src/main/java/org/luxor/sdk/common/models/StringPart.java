package org.luxor.sdk.common.models;

/**
 * @author YanXinMing @date 2024/11/3
 */
public class StringPart extends Part<String> {
    private String contentType;

    public StringPart(String name, String contentType, String value) {
        super(name, value);
        this.contentType = contentType;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }
}
