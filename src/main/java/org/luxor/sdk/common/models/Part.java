package org.luxor.sdk.common.models;


/**
 * Body中的部分参数
 *
 * @author YanXinMing @date 2024/11/3
 */
public class Part<T> {
    private String key;
    private T value;

    public Part(String key, T value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }
}
