package org.luxor.sdk.common.models;


import okhttp3.MediaType;

/**
 * 上传文件
 *
 * @author YanXinMing @date 2024/10/15
 */
public class UploadFile {
    /**
     * 接收属性（如: profile）
     */
    private String name;

    /**
     * 文件名（如: profile.jpg）
     */
    private String fileName;

    /**
     * 文件格式(如：application/octet-stream、image/png 等)
     */
    private MediaType contentType;

    /**
     * 文件内容
     */
    private byte[] content;


}
