package org.luxor.sdk.common.models;

import java.util.HashMap;
import java.util.Map;

/**
 * @author YanXinMing @date 2024/11/3
 */
public class MultipartEntity {

    private final Map<String, Part<?>> multipartParams = new HashMap<>();

    /**
     * Valid only when it's a multipart request object.
     */
    public Map<String, Part<?>> getMultipartRequestParams() {
        return this.multipartParams;
    }

    public void addPart(StringPart part) {
        this.multipartParams.put(part.getKey(), part);
    }

    public void addBinaryPart(BinaryPart part) {
        this.multipartParams.put(part.getKey(), part);
    }

    public void removePart(String key) {
        this.multipartParams.remove(key);
    }

}
