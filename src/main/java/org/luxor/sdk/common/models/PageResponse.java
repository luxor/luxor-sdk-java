package org.luxor.sdk.common.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 分页查询
 *
 * @author Mr.yan  @date 2021/9/13
 */
public class PageResponse<T> implements Serializable {

    /**
     * 当前页数
     */
    @SerializedName("current")
    @Expose
    private Long current;

    /**
     * 每页显示条数
     */
    @SerializedName("size")
    @Expose
    private Long size;

    /**
     * 总记录数
     */
    @SerializedName("total")
    @Expose
    private Long total;

    /**
     * 总页数
     */
    @SerializedName("pages")
    @Expose
    private Long pages;

    /**
     * 数据记录
     */
    @SerializedName("records")
    @Expose
    private List<T> records;

    public PageResponse() {
        this.current = 1L;
        this.size = 10L;
        this.total = 0L;
        this.pages = 0L;
        this.records = new ArrayList<>();
    }

    public Long getCurrent() {
        return current;
    }

    public void setCurrent(Long current) {
        this.current = current;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public Long getPages() {
        return pages;
    }

    public void setPages(Long pages) {
        this.pages = pages;
    }

    public List<T> getRecords() {
        return records;
    }

    public void setRecords(List<T> records) {
        this.records = records;
    }

    @Override
    public String toString() {
        return "PageResponse{" +
                "current=" + current +
                ", size=" + size +
                ", total=" + total +
                ", pages=" + pages +
                ", records=" + records +
                '}';
    }
}
