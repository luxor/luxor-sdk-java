package org.luxor.sdk.common.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * 排序项
 *
 * @author Mr.yan  @date 2021/9/14
 */
public class OrderItem implements Serializable {

    /**
     * 需要进行排序的字段
     */
    @SerializedName("column")
    @Expose
    private String column;

    /**
     * 是否正序排列，默认 true
     */
    @SerializedName("asc")
    @Expose
    private boolean asc = true;

    public OrderItem() {
    }

    public OrderItem(String column, boolean asc) {
        this.column = column;
        this.asc = asc;
    }

    public String getColumn() {
        return column;
    }

    public void setColumn(String column) {
        this.column = column;
    }

    public boolean isAsc() {
        return asc;
    }

    public void setAsc(boolean asc) {
        this.asc = asc;
    }

    @Override
    public String toString() {
        return "OrderItem{" +
                "column='" + column + '\'' +
                ", asc=" + asc +
                '}';
    }
}
