package org.luxor.sdk.common.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 分页查询
 *
 * @author Mr.yan  @date 2021/9/13
 */
public class PageRequest<T> implements Serializable {

    /**
     * 当前页数
     */
    @SerializedName("current")
    @Expose
    private Long current;

    /**
     * 每页显示条数，默认 10
     */
    @SerializedName("size")
    @Expose
    private Long size;

    /**
     * 查询条件
     */
    @SerializedName("search")
    @Expose
    private T search;

    /**
     * 排序条件
     */
    @SerializedName("orders")
    @Expose
    private List<OrderItem> orders;

    public PageRequest() {
        this.size = 10L;
        this.current = 1L;
        this.orders = new ArrayList<>();
    }

    public PageRequest(T search) {
        this();
        this.search = search;
    }

    public PageRequest(T search, OrderItem... orders) {
        this();
        this.search = search;
        this.orders = Arrays.asList(orders);
    }

    public T getSearch() {
        return search;
    }

    public void setSearch(T search) {
        this.search = search;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public long getCurrent() {
        return current;
    }

    public void setCurrent(long current) {
        this.current = current;
    }

    public List<OrderItem> getOrders() {
        return orders;
    }

    public void setOrders(List<OrderItem> orders) {
        this.orders = orders;
    }

    @Override
    public String toString() {
        return "PageRequest{" +
                "search=" + search +
                ", size=" + size +
                ", current=" + current +
                ", orders=" + orders +
                '}';
    }
}
