package org.luxor.sdk.common.models;


/**
 * @author YanXinMing @date 2024/11/3
 */
public class BinaryPart extends Part<byte[]> {
    private final String contentType;

    private final String fileName;

    public BinaryPart(String key, byte[] value, String fileName, String contentType) {
        super(key, value);
        this.fileName = fileName;
        this.contentType = contentType;
    }

    public BinaryPart(byte[] value, String fileName) {
        this("file", value, fileName, "application/octet-stream");
    }

    public String getContentType() {
        return contentType;
    }

    public String getFileName() {
        return fileName;
    }


}
