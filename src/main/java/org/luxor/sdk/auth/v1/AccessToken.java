package org.luxor.sdk.auth.v1;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * 用户访问凭证
 *
 * @author YanXinMing @date 2024/11/6
 */
public class AccessToken {

    /**
     * 访问凭证
     */
    @SerializedName("accessToken")
    @Expose
    public String accessToken;

    /**
     * 凭证类型
     */
    @SerializedName("tokenType")
    @Expose
    public String tokenType;

    /**
     * 有效期(单位:秒)
     */
    @SerializedName("expiresIn")
    @Expose
    public long expiresIn;

    /**
     * 失效时间(单位:毫秒)
     */
    @SerializedName("expiresTimeMs")
    @Expose
    public long expiresTimeMs;

    public String getAuthorization() {
        return tokenType + " " + accessToken;
    }
}
