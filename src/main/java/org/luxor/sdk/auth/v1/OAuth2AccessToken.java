package org.luxor.sdk.auth.v1;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * 应用访问凭证
 *
 * @author YanXinMing @date 2024/11/6
 */
public class OAuth2AccessToken {

    /**
     * 访问凭证
     */
    @SerializedName("access_token")
    @Expose
    public String accessToken;

    /**
     * 凭证类型
     */
    @SerializedName("token_type")
    @Expose
    public String tokenType;

    /**
     * 续期凭证
     */
    @SerializedName("refresh_token")
    @Expose
    public String refreshToken;

    /**
     * 有效期(单位:秒)
     */
    @SerializedName("expires_in")
    @Expose
    public long expiresIn;

    /**
     * 授权范围
     */
    @SerializedName("scope")
    @Expose
    public String scope;
}
