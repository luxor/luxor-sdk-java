package org.luxor.sdk.common;

import org.junit.Test;
import org.luxor.sdk.common.exception.LuxorCloudSDKException;
import org.luxor.sdk.common.profile.ClientProfile;
import org.luxor.sdk.common.profile.HttpProfile;
import org.luxor.sdk.common.profile.HttpProtocol;

/**
 * @author Mr.yan  @date 2021/9/13
 */
public class CommonUserClientTest {

    @Test
    public void getUserInfoTest() throws LuxorCloudSDKException {
        Credential credential = new Credential("1001", "123456");

        ClientProfile clientProfile = new ClientProfile();
        clientProfile.setDebug(true);
        clientProfile.setHttpProfile(new HttpProfile(HttpProtocol.HTTP, "127.0.0.1", 81));

        CommonUserClient client = new CommonUserClient("1.0", credential, "guangdong", clientProfile);
        String response = client.getRequest("/system/sysUser/principal", null);

        System.out.println(response);
    }

}
