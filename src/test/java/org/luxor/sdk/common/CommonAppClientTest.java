package org.luxor.sdk.common;

import org.junit.Test;
import org.luxor.sdk.common.exception.LuxorCloudSDKException;
import org.luxor.sdk.common.profile.ClientProfile;
import org.luxor.sdk.common.profile.HttpProfile;
import org.luxor.sdk.common.profile.HttpProtocol;

/**
 * @author Mr.yan  @date 2021/9/13
 */
public class CommonAppClientTest {
    @Test
    public void getUserInfoTest() throws LuxorCloudSDKException {
        Credential credential = new Credential("1001", "123456");

        ClientProfile clientProfile = new ClientProfile();
        clientProfile.setDebug(true);
        clientProfile.setHttpProfile(new HttpProfile(HttpProtocol.HTTP, "127.0.0.1", 81));

        CommonAppClient client = new CommonAppClient("1.0", credential, "guangdong", clientProfile);
        String response = client.getRequest("/token/user_info", null);
        System.out.println(response);
    }
}
